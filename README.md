# rsxc-b64-32-app



## Getting started

To run this program, simply compile it an run the executable. Given the question at the start of execution, put "encrypt" or "decrypt" on the prompt and follow the steps described in the program.


## Name
This implements the *R*olling-*S*hift-*X*or-*C*ypher (by Guilherme Duarte Silva Matos) with Base key of 64 characters and hash of 32 characters (stupid hasher hash by Guilherme Duarte Silva Matos) in a terminal app. Therefore: rsxc-b64-h32-app.

## Description
Encrypts text in a way that *hopefully* can't be broken/decrypted without the key. It's fairly slow, but if properly secure, could assure safe transaction of data. Keys are one time keys, based on the data that they encrypt. As the keys themselves depend on both the content and lenght of the Data, and having the encrypted data only provides the lenght of the text, it should be pretty tough to crack.

## Installation
working on it

## Roadmap
make encryption concurrent for each chunk.

## Contributing
Well, not expecting much, but any contribution is apreciated!

## Authors and acknowledgment
Myself (Guilherme Duarte Silva Matos)

## Project status
Waiting on feature request. 
