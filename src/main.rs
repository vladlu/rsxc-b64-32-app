use std::io::stdin;
use std::char::*;
use std::char::REPLACEMENT_CHARACTER;
use std::io;
//use std::env::args;
use rand::prelude::*;

fn io_read() -> io::Result<String> {
    let mut result = String::new();
    stdin().read_line(&mut result)?;
    Ok(result.trim().to_owned())
}

fn main() {
    println!("encrypt or decrypt?");
     let input = match io_read() {
        Ok(s) => s,
        Err(e) => {
            println!("Failed with: {}",e);
            std::process::exit(1)
        },
    };
    match input.as_ref() {
        "encrypt" => run_encrypt(),
        "decrypt" => run_decrypt(),
        _ => unreachable!()
    }
}

fn run_decrypt() {
    println!("Your encrypted text:");
    let text = match io_read() {
        Ok(s) => s,
        Err(e) => {
            println!("Failed with: {}",e);
            std::process::exit(1)
        },
    };
    println!("Your generated key:");
    let key = match io_read() {
        Ok(s) => s,
        Err(e) => {
            println!("Failed with: {}",e);
            std::process::exit(1)
        },
    };
    let mut decrypted_all = "".to_owned();
    let keyv = key.chars().collect::<Vec<char>>().chunks(64).map(|x| x.iter().collect()).collect::<Vec<String>>();
    let textv = text.chars().collect::<Vec<char>>().chunks(61).map(|x| x.iter().collect()).collect::<Vec<String>>();
    for i in 0..keyv.len() {
        decrypted_all = format!("{}{}",decrypted_all,decrypt(keyv[i].clone(), textv[i].clone()));
    }
    println!("  Your original text:   {}", decrypted_all);
}

fn run_encrypt() {
    println!("Insert your input:");
    let input = match io_read() {
        Ok(s) => s,
        Err(e) => {
            println!("Failed with: {}",e);
            std::process::exit(1)
        },
    };
    let lenght :u64 = input.len() as u64;
    let key_size :u64 = (lenght.pow(2) % 8 + 1) * (lenght.pow(lenght as u32 % 4) % 256);
    let signer = if key_size > lenght {
        format!("{:03}", ((key_size*lenght) / gcd(key_size,lenght)) % 1000)
    } else {
        format!("{:03}",((key_size*lenght) / gcd(lenght,key_size)) % 1000)
    };
    let values = input.chars().collect::<Vec<char>>().chunks(61).map(|x| x.to_vec()).collect::<Vec<Vec<char>>>();
    let mut input_hashv = vec![];
    for val in values.iter() {
        input_hashv.push(stupid_hasher(val.iter().cloned().collect(), signer.clone()));
    }
    //let mut tmp = [0;32];
    //for hval in input_hashv.iter() {
    //    let v_val = hval.chars().collect::<Vec<char>>();
    //    for i in 0..32 {
    //        tmp[i] += v_val[i] as u32;
    //    }
    //}
    let mut keyv = vec![];
    for input_hash in input_hashv {
        let hash_array = input_hash.chars().map(|x| x as u32).collect::<Vec<u32>>();
        let mut last32 = ['\0';32];
        for i in 0..32 {
            last32[i] = get_key_char(hash_array[i], hash_array[(i+1)%32],0);
        }
        keyv.push(hash_array.into_iter().map(|x| from_u32(x).unwrap_or('a')).chain(last32.into_iter()).collect::<String>());
    }

    let mut final_key = "".to_owned();
    let mut final_encrypted = "".to_owned();
    for i in 0..values.len() {
        let (encrypted,finalk) = encrypt(keyv[i].clone(), values[i].iter().cloned().collect());
        final_key = format!("{}{}",final_key,finalk);
        final_encrypted = format!("{}{}",final_encrypted,encrypted);
        
    }  
    //println!("{}",&input_hash);
    
    //let (bn1,bn2) = ((random::<f64>() * 1000000000f64) as u32 + 1,(random::<f64>() * 1000000000f64) as u32 + 1);
    //let o_signer = format!("{:03}",((bn1*bn2) / gcd(bn1 as u64,bn2 as u64) as u32) % 1000);
    //let rest32 = stupid_hasher(last32.iter().collect(), signer);
    ////println!("final_k_hash  : {}",&final_key);
    println!("  encrypted     : {}", final_encrypted);
    println!("  final_key     : {}", final_key);
}

fn encrypt(key:String,text:String) -> (String,String) {
    let mut res : Vec<char> = text.clone().chars().collect();
    let mut key_ch : Vec<char>  = key.clone().chars().collect();
    let mut signer : Vec<char>= key_ch.iter().cloned().take(3).collect();
    let mut ev_ok = false;
    let mut counter = 0;
    let mut sub_char = 'a';
    let res_len = res.len();
    while !ev_ok {
        if key.len() > res.len() {
            for i in 0..key_ch.len() {
               if (i%3) == 0 {
                    res[i%res_len] = from_u32(signer[0] as u32 ^ key_ch[i] as u32 ^ res[i%res_len] as u32).unwrap_or(REPLACEMENT_CHARACTER);
                } else if (i%3) == 1 {
                    res[i%res_len] = from_u32(signer[1] as u32 ^ key_ch[i] as u32 ^ (res[i%res_len] as u32)).unwrap_or(REPLACEMENT_CHARACTER);
                } else {
                    res[i%res_len] = from_u32(signer[2] as u32 ^ (key_ch[i] as u32 ^ res[i%res_len] as u32)).unwrap_or(REPLACEMENT_CHARACTER);
                } 
            }
        } else {
            for i in 0..res.len() {
                if (i%3) == 0 {
                    res[i] = from_u32(signer[0] as u32 ^ key_ch[i%64] as u32 ^ res[i] as u32).unwrap_or(REPLACEMENT_CHARACTER);
                } else if (i%3) == 1 {
                    res[i] = from_u32(signer[1] as u32 ^ key_ch[i%64] as u32 ^ (res[i] as u32)).unwrap_or(REPLACEMENT_CHARACTER);
                } else {
                    res[i] = from_u32(signer[2] as u32 ^ key_ch[i%64] as u32 ^ res[i] as u32).unwrap_or(REPLACEMENT_CHARACTER);
                }
            }
        }
        ev_ok = true;
        for cha in res.iter() {
            if !((cha.is_alphanumeric() || cha.is_ascii_punctuation() || cha.is_ascii_graphic()) && !cha.is_control() && !cha.is_whitespace()) {
                ev_ok = false;
                //println!("Text Fail!");
            }
        }
        for cha in key_ch.iter() {
            if  cha.is_control() || cha.is_whitespace() {
                ev_ok = false;
                //println!("Key Fail!");
            }
        }
        if !ev_ok {
            res = text.clone().chars().collect();
            for i in 0..key_ch.len() {
                if (counter % 64) == 0 {
                    sub_char = get_key_char(32, 1024, 0);
                }
                key_ch[i] = from_u32(key_ch[(i+1)%key_ch.len()] as u32 ^ sub_char as u32).unwrap_or('a'); 
            }

            signer = key_ch.iter().cloned().take(3).collect();
        }
        counter += 1;
    }
    (res.iter().collect(),key_ch.iter().collect())
}

fn decrypt(key: String, text: String) -> String {
    let mut res : Vec<char> = text.clone().chars().collect();
    let key_ch : Vec<char>  = key.clone().chars().collect();
    let signer : Vec<char>= key_ch.iter().cloned().take(3).collect();
    let text_lenght = res.len();
    //println!("{}",text_lenght);
    if key.len() > text.len() {
        for i in 0..key_ch.len() {
           if (i%3) == 0 {
                res[i%text_lenght] = from_u32(signer[0] as u32 ^ key_ch[i] as u32 ^ res[i % text_lenght] as u32).unwrap_or(REPLACEMENT_CHARACTER);
            } else if (i%3) == 1 {
                res[i%text_lenght] = from_u32(signer[1] as u32 ^ key_ch[i] as u32 ^ (res[i % text_lenght] as u32)).unwrap_or(REPLACEMENT_CHARACTER);
            } else {
                res[i%text_lenght] = from_u32(signer[2] as u32 ^ (key_ch[i] as u32 ^ res[i % text_lenght] as u32)).unwrap_or(REPLACEMENT_CHARACTER);
            } 
        }
    } else {
        for i in 0..text.len() {
            if (i%3) == 0 {
                res[i] = from_u32(signer[0] as u32 ^ key_ch[i%64] as u32 ^ res[i] as u32).unwrap_or(REPLACEMENT_CHARACTER);
            } else if (i%3) == 1 {
                res[i] = from_u32(signer[1] as u32 ^ key_ch[i%64] as u32 ^ (res[i] as u32)).unwrap_or(REPLACEMENT_CHARACTER);
            } else {
                res[i] = from_u32(signer[2] as u32 ^ key_ch[i%64] as u32 ^ res[i] as u32).unwrap_or(REPLACEMENT_CHARACTER);
            }
        }
    }
    res.iter().collect()
}


fn get_key_char(left: u32, right: u32,offst: u32) -> char {
    let min_lf = left.min(right);
    let max_lf = left.max(right);
    let diff = max_lf - min_lf;
    let rng: u32 = (random::<f64>() * diff as f64) as u32;
    let rangee = (min_lf..=max_lf).collect::<Vec<u32>>();
    let cha = from_u32(rangee[rng as usize] + offst).unwrap_or('a');  
    if (cha.is_alphanumeric() || cha.is_ascii_punctuation() || cha.is_ascii_graphic()) && !cha.is_control() && !cha.is_whitespace() {
        cha
    } else {
        get_key_char(left,right,offst + 32)
    }
}

fn stupid_hasher(text:String, signer: String) -> String {
    let valid_area_1 = ((u32::from_str_radix("0", 16).unwrap())..(u32::from_str_radix("D800",16).unwrap())).collect::<Vec<u32>>();
    let valid_area_2 = ((u32::from_str_radix("DFFF",16).unwrap()+1)..=(u32::from_str_radix("10FFFF", 16).unwrap())).collect::<Vec<u32>>();
    let valid_area = valid_area_1.into_iter().chain(valid_area_2.into_iter()).collect::<Vec<u32>>();
    let signer_chars = signer.chars().collect::<Vec<char>>();
    let mut text_bytes = text.chars().map(|ch| ch as u32).collect::<Vec<u32>>();
    let mut arr : [u32;29] = [0;29];
    //println!("signer    : {:?} <:: {} ", signer_bytes, signer);
    //println!("text      : {:?} <:: {}", text_bytes, text);
    for c in signer_chars.iter() {
        match *c {
            '0' => {
                for (i,b) in text_bytes.iter().enumerate() {
                    arr[i%29] += b
                } 
            }
            '1' => {
                text_bytes.reverse();
                for (i,_) in text_bytes.iter().enumerate() {
                    arr[i%29] += 1
                }
            }
            '2' => {
               for i in 0..arr.len() {
                    arr[i] = arr[i] ^ (i+1) as u32 
                }
            }
            '3' => {
                for i in 0..arr.len() {
                    arr[i] += arr[(i+1)%29]
                }
            }
            '4' => {
                for i in 0..arr.len() {
                    arr[i] = arr[i] ^ arr[(i+1)%29]
                }
            }
            '5' => {
                text_bytes = text_bytes.into_iter().map(|x| x << 2).collect()
            }
            '6' => {
                for (i,b) in text_bytes.iter().enumerate() {
                    if (b % 2) == 1 {
                        arr[i%29] += b
                    } else {
                        arr[i%29] /= 2
                    }
                }
            }
            '7' => {
                for (i,b) in text_bytes.iter().enumerate() {
                    if (b % 3) == 0 {
                        arr[i%29] /= 3
                    } else if (b%3) == 1 {
                        arr[i%29] <<= 2
                    } else {
                        if (i % 2) == 0 {
                            arr[i%29] /= (gcd(arr[i%29] as u64, arr[(i+1)%29] as u64) as u32) + 1 
                        } else {
                            arr[i%29] += i as u32 + b 
                        }
                    }
                }
            }
            '8' => {
                for i in 0..arr.len() {
                    arr[i] ^= arr[(i+1)%29] / (1+gcd(arr[(i+1)%29] as u64, arr[(i*2)%29] as u64) as u32); 
                }
            }
            '9' => {
                for i in 0..arr.len() {
                    arr[i] %= 256
                }
            }
            _ => unreachable!()
        }
    }
    let mut ch_arr = ['\0';29];
    for i in 0..arr.len(){
        arr[i] += 32;
        if valid_area.contains(&arr[i]) {
            ch_arr[i] = from_u32(arr[i]).unwrap_or(char::REPLACEMENT_CHARACTER);
        } else {
            ch_arr[i] = from_u32(arr[i]%55295).unwrap_or(char::REPLACEMENT_CHARACTER);
        }
        if ch_arr[i].is_control() {
            ch_arr[i] = from_u32(ch_arr[i] as u32 + 35u32).unwrap_or(REPLACEMENT_CHARACTER);
        }
        if ch_arr[i].is_whitespace() {
            ch_arr[i] = from_u32(ch_arr[i] as u32 + 35u32).unwrap_or(REPLACEMENT_CHARACTER); 
        }
        if (768u32..880u32).contains(&arr[i]) {
            ch_arr[i] = from_u32(ch_arr[i] as u32 + (880 - 768)).unwrap_or(REPLACEMENT_CHARACTER); 
        }
    }
    //println!("arr       : {:?}",arr);
    //println!("ch_arr    : {:?}",&ch_arr);
    //println!("codedStr  : {}", ch_arr.iter().cloned().collect::<String>());
    let hash = signer_chars.into_iter().chain(ch_arr.into_iter()).collect::<String>();
    hash
}

fn gcd(a: u64,b: u64) -> u64 {
    if b == 0 {
        return a;
    }
    let n = a - b * (a /b);
    gcd(b,n)
}


// 3chr signer // 29chr HASHARRAY // 
